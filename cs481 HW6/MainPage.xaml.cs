﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace cs481_HW6
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //detect internet/wifi connection +7 points
        public bool DoIHaveInternet()
        { return CrossConnectivity.Current.IsConnected; }

        //user has completed their search query, hitting search
        async void searchBar_Completed(object sender, EventArgs e)
        {
            //part of bad Json/no internet handling, part of +5 points
            if (!DoIHaveInternet())
            {
                //no internet, tell user to get some.
                OutPutText.Text = "Could not reach Dictionary, network unavailable. Please try again once network is available.";
                OutPutText.TextColor = Color.Red; //turn the text red to spook em good
                return;
            }
            else //we have internet
            {
                OutPutText.Text = "Consulting the wise owl.."; //tell the user what is going on.
                OutPutText.TextColor = Color.Default; //turn the text not red
            }

            var text = ((Entry)sender).Text; //our searched text

            //black magic networking things occur, getting the JSON +10 points
            HttpClient client = new HttpClient();
            
            //the ritual is prepared, and incantations readied.
            var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + $"{text}"));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;

            HttpResponseMessage response = await client.SendAsync(request); //activate the ritual and contact the powers beyond the veil

            if (response.IsSuccessStatusCode) //successfully made contact with those born of the outer spheres.
            {
                var content = await response.Content.ReadAsStringAsync(); //transcribe recieved knowledge
                var wordDefinition = WordDefinition.FromJson(content); //render it safe for comprehension by mortal minds.
                string outputString = ""; //initiate output string.

                //make sure we display all the definitions, loop em. 
                for (int i=0;i<wordDefinition.Length;i++)
                {
                    outputString += "" + text + ":" + '\n' + "(" + wordDefinition[i].Type + ")" + '\n' + wordDefinition[i].Definition + '\n' + "Example: " + '"' + wordDefinition[i].Example + '"' + '\n' + '\n'; //Display type +1 points. Display definition +1 points. Display example +1 points.
                }

                OutPutText.Text = outputString; //set the output text
            }
            else //the sorcerous ritual failed. other part of error handling. part of +5 points.
            {
                OutPutText.Text = "Get from API failed. Is this a dictionary word?";
            }
        }
    }

    //all of this is generated via the "quicktype" source from the week 9 powerpoint. I have no hand in its creation outside of pasting a json and playing with namespaces.
    public partial class WordDefinition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class WordDefinition
    {
        public static WordDefinition[] FromJson(string json) => JsonConvert.DeserializeObject<WordDefinition[]>(json, cs481_HW6.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this WordDefinition[] self) => JsonConvert.SerializeObject(self, cs481_HW6.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
